var app = getApp()
Page({
    data: {
        resource:'',
        hasAddress: null,
        address: [],
        goods:[],
        totalPrice: 0,
        send_fee:0,
        service_fee:0,
        orderList: [],
        curAddressData: app.globalData.curAddressData,
        isNeedLogistics: 0, // 是否需要物流信息
        shopId: 0,
        hasNoCoupons: true,
        youhuijine: 0, //优惠券金额
        
        curCoupon: null, // 当前选择使用的优惠券
        
        attachment: '',
        shop: [],
        pay_type: '',
        send_type:'',
        array: ['微信支付', '货到付款'],
        objectArray: [{
                id: 1,
                name: '微信支付'
            },
            {
                id: 2,
                name: '货到付款'
            }
        ],
    },

    onLoad: function(e) {
        var that = this;
        app.tools.commonSet(app)
        that.setData({
            resource: app.globalData.resource
        })
        that.loadGoods(e);
        // that.setData({
        //     isNeedLogistics: 1,
        //     shopId: shopid,
        //     shop: shop,
        //     totalPrice: parseFloat(allPrice.toFixed(2)),
        //     total: _orderList.total,
        //     count: _orderList.count,
        //     orderList: cartList,
        //     attachment: app.globalData.attachment
        // });

    },
    onShow: function() {

        var that = this;
        app.checkToken()
        that.loadAddress();
        that.loadDispatch()
        that.loadPayment()
    },
    loadAddress: function() {
        var that = this;
        app._http('address/default','','get',function(res){
            if(res.data.status == 1){
                that.setData({
                    hasAddress:true,
                    address:res.data.data
                })
            }else{
                that.setData({
                    hasAddress: false,
                })
            }
        });
    },
    loadGoods:function(cart){
        var that = this
        app._http('order/loadcarts', { cart_ids: cart }, 'post', function (res) {
            console.log(res)
            if(res.data.status == 0){
                wx.showToast({
                    title: '无可购买的商品',
                    icon: 'none',
                    duration: 2000,
                    success:function(){
                        wx.navigateBack({
                            
                        })
                    }
                })
            }
            that.setData({
                goods:res.data.data.goods,
                total: res.data.data.price.toFixed(2),
                // send_fee:5.00,
                // service_fee:0.00,
                // send_type:'send',
                // pay_type:'money',
                // totalPrice: (res.data.total + 5.00 + 0.00).toFixed(2)
            })
        })
    },

    loadDispatch: function () {
        var that = this;
        app.http('dispatch/index', '', 'get', function (res) {
            
        });
    }, 

    loadPayment: function () {
        var that = this;
        app.http('payment/index', '', 'get', function (res) {

        });
    },

    payChange: function(e) {
        var that = this;
        let pay_type = e.detail.value
        let totalPrice = that.data.totalPrice
        let service_fee = 1.00;
        if(pay_type == 'money'){
            totalPrice = totalPrice + service_fee; 
            console.log(totalPrice)
            that.setData({
                pay_type: pay_type,
                totalPrice: totalPrice.toFixed(2),
                service_fee:1.00
            })
        }else{
            totalPrice = totalPrice - 1;
            that.setData({
                pay_type: pay_type,
                totalPrice: totalPrice.toFixed(2),
                service_fee: 0.00
            })
        }
    },
    sendChange: function (e) {
        var that = this;
        let send_type = e.detail.value
        let totalPrice = that.data.totalPrice
        if (send_type == 'self') {
            totalPrice = totalPrice - 5
            that.setData({
                send_type: send_type,
                totalPrice: totalPrice.toFixed(2),
                send_fee:0.00
            })
        }else{
            totalPrice = totalPrice + 5
            that.setData({
                send_type: send_type,
                totalPrice: totalPrice.toFixed(2),
                send_fee: 5.00
            })
        }
    },

    createOrder: function(e) { //创建订单
        wx.showLoading();
        console.log(e)
        var that = this;
        let goods = that.data.goods;
        var remark = e.detail.value.remark; 
        var pay_type = e.detail.target.dataset.pay_type;
        var send_type = e.detail.target.dataset.send_type;
        var totalPrice = e.detail.target.dataset.totalprice;
        if (that.data.address == '') {
            wx.hideLoading();
            wx.showModal({
                title: '错误',
                content: '请先设置您的地址！',
                showCancel: false
            })
            return;
        }
        var address = that.data.address
        var formid = e.detail.formId;
        console.log(totalPrice)
        for(var i = 0; i < goods.length; i++ ){
            delete goods[i].goods;
        }
        var data = {
            'goods': goods,
            'send_type': 1,
            'send_fee': that.data.send_fee,
            'price': totalPrice,
            'total_price': totalPrice,
            'remark': remark,
            'address': address,
            'pay_type': 1,
        };
        app._http('order/create',data,'post',function(res){
            wx.hideLoading();
            console.log(res);
            return;
            if(res.data.status == 0){
                wx.showModal({
                    title: '提示',
                    content: '订单提交失败',
                    showCancel: false,
                    success: function (_res) {
              
                    }
                })
            }else{
                wx.showModal({
                    title: '提示',
                    content: '订单提交成功',
                    showCancel: false,
                    success: function (_res) {
                        if (_res.confirm) {
                            wx.navigateTo({
                                url: '../order_list/order_success?id=' + res.data.orderid
                            })
                        }
                    }
                })
            }
            
        })

    },
    addAddress: function() {
        var that = this;
        wx.navigateTo({
            url: '/app/pages/address/index'
        })
    },
    getMyCoupons: function() {
        var that = this;
    },
    bindChangeCoupon: function(e) {
        const selIndex = e.detail.value[0] - 1;
        if (selIndex == -1) {
            this.setData({
                youhuijine: 0,
                curCoupon: null
            });
            return;
        }
        this.setData({
            youhuijine: this.data.coupons[selIndex].reduce_cost / 100,
            curCoupon: this.data.coupons[selIndex]
        });
    }
})