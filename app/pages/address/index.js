 var app = getApp()
Page({
    data: {
        addressList: [],
        isMoRen:true
        
    },
   
    onLoad: function() {
        app.tools.commonSet(app)
    },
    onShow: function() {
        var that = this
        app.checkToken()
        that.loadAddress();
    },
    loadAddress: function() {
        var that = this;
        app._http('address/index','','get',function(res){
            that.setData({
                addressList: res.data.data
            })
        });
    },
    addAddess: function () {
        wx.navigateTo({
            url: '/app/pages/address/address_edit'
        })
    },
    editAddess: function (e) {
        wx.navigateTo({
            url: "/app/pages/address/address_edit?id=" + e.currentTarget.dataset.id
        })
    },
    use:function(e){
        var id = e.currentTarget.dataset.id
        app.http('address_use',{id:id},'put',function(res){
            if(res.data.status == 0){
                wx.showToast({
                    title: '使用失败',
                    duration: 1000
                })
            }
            wx.showToast({
                title: '使用成功',
                duration: 1000,
                success:function(){
                    wx.navigateBack({
                        delta: 1,
                    })
                }
            })
        })
    },

    
    defaultFun(e){
        var that = this;
        let id = e.target.dataset.id;
        console.log(e);
        console.log(id);
        let index = e.target.dataset.index
        
        app._http('address/default/' + id,'','put',function(res){
            console.log(res)
            if (res.statusCode == 201){
              wx.showToast({
                  title: '设置成功',
              })
                for (var i = 0; i < that.data.addressList.length;i++){
                    if(i == index){
                        that.data.addressList[index].is_default = 1;
                    }
                    else{
                        
                        that.data.addressList[i].is_default = 0;
                    }
                }
                that.setData({
                    addressList: that.data.addressList
                })
                
            }
            else{
                wx.showToast({
                    title: '设置失败',
                    icon:"none"
                })
            }

        })
    },
    deleteFun(e) {
        var that = this;
        let id = e.target.dataset.id;
        let index = e.target.dataset.index
        console.log(index)
        wx.showModal({
            title: '提示',
            content: '确认删除吗？',
            success:function(resConfirm){
                if (resConfirm.confirm){
                    app._http('address/destroy/' + id, '', 'delete', function (res) {
                    
                        if (res.statusCode == 201) {
                            wx.showToast({
                                title: '删除成功',
                            })
                            that.data.addressList.splice(index, 1)
                            that.setData({
                                addressList: that.data.addressList
                            })
                        }
                        else {
                            wx.showToast({
                                title: '删除失败',
                                icon: "none"
                            })
                        }

                    })
                }
            }
            
        })
       
    }
})