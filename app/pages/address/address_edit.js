var commonCityData = require('../../resource/utils/city.js');
var app = getApp()
Page({
    data: {
        address: '',
        lat: '',
        lng: '',
        id: '',
    },
    onLoad: function(e) {
        var that = this;
        app.tools.commonSet(app)
        if (e.id) {
            that.showAddress(e.id);
        }
    },
    onShow: function() {
        var that = this
        app.checkToken()
    },

    bindCancel: function() {
        wx.navigateBack({})
    },

    getaddress: function() {
        var that = this;
        wx.chooseLocation({
            success: function(res) {
                console.log(res)
                that.setData({
                    address: res.address + res.name,
                    lat: res.latitude,
                    lng: res.longitude
                })
            }
        });
    },
    bindSave: function(e) {
        var that = this;
        var id = e.detail.target.dataset.id;
        var lat = e.detail.target.dataset.lat;
        var lng = e.detail.target.dataset.lng;
        var name = e.detail.value.name;
        var address = e.detail.value.address;
        var phone = e.detail.value.phone;
        var floor = e.detail.value.floor;
        if (address == "") {
            wx.showModal({
                title: '提示',
                content: '请选择收货地址',
                showCancel: false
            })
            return
        }
        if (name == "") {
            wx.showModal({
                title: '提示',
                content: '请填写联系人姓名',
                showCancel: false
            })
            return
        }
        if (phone == "") {
            wx.showModal({
                title: '提示',
                content: '请填写手机号码',
                showCancel: false
            })
            return
        }
        wx.showLoading({
            title: '保存中',
        })
        if (id == "") {
            app._http('address/store', {
                    name: name,
                    address: address,
                    floor: floor,
                    phone: phone,
                    lat: lat,
                    lng: lng
                }, 'post', function(res) {
                    console.log(res)
                    if (res.statusCode == 500) {

                        console.log(res.data.errors)
                        var errorsMsg = res.data.errors;
                        var msg = ""
                        for (var p in errorsMsg) {
                            console.log(p + "" + errorsMsg[p]);
                            msg = errorsMsg[p][0];
                        }



                        wx.showToast({
                            title: msg,
                            icon: 'none',
                            duration: 2000
                        })
                        return;
                    }
                    if (res.statusCode == 201) {
                        wx.showToast({
                            title: '添加成功',
                            icon: 'success',
                            duration: 1000,
                            success: function() {
                                wx.navigateTo({
                                    url: './index'
                                })
                            }
                        })
                    } else {
                        wx.showToast({
                            title: '添加失败',
                            icon: 'none',
                            duration: 2000
                        })
                    }
                },
                function(res) {
                    console.log(res);
                })
        } else {
            app._http('address/save/' + id, {
                name: name,
                address: address,
                floor: floor,
                phone: phone,
                lat: lat,
                lng: lng
            }, 'put', function(res) {
                if (res.statusCode == 201) {
                    wx.showToast({
                        title: '修改成功',
                        icon: 'success',
                        duration: 1000,
                        success: function() {
                            wx.navigateTo({
                                url: '/app/pages/address/index'
                            })
                        }
                    })
                } else {
                    wx.showToast({
                        title: '修改失败',
                        icon: 'none',
                        duration: 2000
                    })
                }
            })
        }

    },

    showAddress: function(id) {
        var that = this;
        app._http('address/edit/' + id, '', 'get', function(res) {
            var address = res.data
            that.setData({
                address: address.address,
                phone: address.phone,
                floor: address.floor,
                name: address.name,
                id: address.id,
                lat: address.lat,
                lng: address.lng
            })
        })
    },

    deleteAddress: function(e) {
        var that = this;
        var id = e.currentTarget.dataset.id;
        wx.showModal({
            title: '提示',
            content: '确定要删除该收货地址吗？',
            success: function(res) {
                if (res.confirm) {
                    app._http('address/destroy' + id, '','delete',function(res){
                        if (res.statusCode == 201) {
                            wx.showToast({
                                title: '删除成功',
                                icon: 'success',
                                duration: 1000,
                                success: function () {
                                    wx.navigateTo({
                                        url: '/app/pages/address/index'
                                    })
                                }
                            })
                        } else {
                            wx.showToast({
                                title: '删除成功',
                                icon: 'none',
                                duration: 2000
                            })
                        }
                    })

                } else if (res.cancel) {
                    // console.log('用户点击取消')
                }
            }
        })
    }
})