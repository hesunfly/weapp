var app = getApp()
Page({
  data: { coupons:[] },
  onLoad: function (options) {
    let that = this

    wx.request({
      url: app.util.url('/entry/wxapp/GetMyCoupons/'),
      method: 'GET',
      data: {
        m: 'app'
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
      if (res.data.errno == 0) {
          that.setData({
            coupons: res.data.data
          })
        }

      }
    })
  },
  use:function(){
    var shopid= e.currentTarget.dataset.shopid;
    if (shopid>0){
      wx.navigateBack({
        url: '../shop/shop?id='+shopid,
      })
    }else{
      wx.navigateBack({
        url: '../index/index',
      })
    }
  },
  getReceivedData: function (jsonArr) {
    let dataset = new Array();
    let len = jsonArr.length;
    for (let i = 0; i < len; i++) {
      dataset.push(jsonArr[i].couponId);
    }
    return dataset;
  },
  classify: function (origin, comp) {
    let received = new Array();
    let unreceived = new Array();
    let len = origin.length;
    for (let i = 0; i < len; i++) {
      if (comp.indexOf(origin[i].id) === -1) {
        unreceived.push(origin[i]);
      } else {
        received.push(origin[i]);
      }
    }
    this.setData({
      received: received,
      unreceived: unreceived
    });
  },
  //领取优惠券
  draw: function (e) {
    var that = this
    let index = e.currentTarget.dataset.index;
    let couponid = this.data.unreceived[index].id

    wx.request({
        url: app.util.url('/entry/wxapp/ApiDrawCoupons/'),
        header: { "Content-Type": "application/x-www-form-urlencoded" },
        method: "POST",
        data: {
          couponID: couponid
        },
        success: function (res) {
          console.log(res)
          if (res.data.errno == 0) {
            wx.showModal({
              title: '提示',
              content: res.data.message,
              showCancel: false,
              success: function (_res) {
                that.onLoad();
              }
            })
          }

        }
    })


    let unreceived = this.data.unreceived;
    let received = this.data.received;
    received.push(unreceived[index]);
    unreceived.splice(index, 1);

    this.setData({
      received: received,
      unreceived: unreceived
    });
  },
  onReady: function () {
    wx.setNavigationBarTitle({
      title: '优惠券 '
    })
  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  }
})