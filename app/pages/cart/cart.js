var app = getApp();
Page({
    data: {
        carts: [], // 购物车列表
        hasList: false, // 列表是否有数据
        totalPrice: 0, // 总价，初始为0
        selectAllStatus: false, // 全选状态，默认全选

    },
    onLoad() {
        var that = this;
        app.tools.commonSet(app)
        that.setData({
            resource: app.globalData.resource
        })
    },
    onShow() {
        var that = this
        app.checkToken()
        that.loadCart()
        that.getTotalPrice();
    },
    //加载购物车数据
    loadCart:    function() {
        var that = this;
        app._http('cart/index', '', 'get', function(res) {
            if (res.statusCode != 200) {
                app._http('cart/index', '', 'get', function (res) {
                    if (res.data.data != '') {
                        that.setData({
                            hasList: true,
                            carts: res.data.data
                        })
                    }
                });
            }else{
                if (res.data.data != '') {
                    that.setData({
                        hasList: true,
                        carts: res.data.data
                    })
                }
            }
        });
    },

    //结算
    account:function(){
        let carts = this.data.carts; // 获取购物车列表
        let total = 0;
        var cart_ids = [];
        for (let i = 0; i < carts.length; i++) { // 循环列表得到每个数据
            if (carts[i].selected) { // 判断选中才会计算价格
                cart_ids.push(carts[i].id);
            }
        }
        if(cart_ids.length == 0){
            wx.showToast({
                title: '没有选中商品',
                icon: 'none',
                duration: 2000
            })
            return
        }
        wx.navigateTo({
            url: '/app/pages/to-pay-order/pay_order?cart_ids=' + cart_ids,
        })
        
    },

    /**
     * 当前商品选中事件
     */
    selectList(e) {
        const index = e.currentTarget.dataset.index;
        let carts = this.data.carts;
        const selected = carts[index].selected;
        carts[index].selected = !selected;
        this.setData({
            carts: carts
        });
        this.getTotalPrice();
    },

    /**
     * 删除购物车当前商品
     */
    deleteList(e) {
        var that = this;
        const index = e.currentTarget.dataset.index;
        const id = e.currentTarget.dataset.id;
        app._http('cart/' + id + '/destroy', '', 'delete', function(res) {
            if (res.data.status == 1) {
                wx.showToast({
                    title: '删除成功',
                    icon: 'success',
                    duration: 1000
                })

                let carts = that.data.carts;
                carts.splice(index, 1);
                that.setData({
                    carts: carts
                });
                if (!carts.length) {
                    that.setData({
                        hasList: false
                    });
                } else {
                    that.getTotalPrice();
                }
            } else {
                wx.showToast({
                    title: '删除失败',
                    icon: 'none',
                    duration: 1000
                })
            }
        })

    },

    /**
     * 购物车全选事件
     */
    selectAll(e) {
        let selectAllStatus = this.data.selectAllStatus;
        selectAllStatus = !selectAllStatus;
        let carts = this.data.carts;

        for (let i = 0; i < carts.length; i++) {
            carts[i].selected = selectAllStatus;
        }
        this.setData({
            selectAllStatus: selectAllStatus,
            carts: carts
        });
        this.getTotalPrice();
    },

    /**
     * 绑定加数量事件
     */
    addCount(e) {
        var that = this
        const index = e.currentTarget.dataset.index;
        const id = e.currentTarget.dataset.id;
        const goods_id = e.currentTarget.dataset.goods_id;
        let carts = that.data.carts;
        let count = carts[index].count;
        
        count = count + 1;
      
        app._http('cart/' + id + '/update', {
            goods_id: goods_id,
            count: count
        }, 'put', function(res) {

            if (res.statusCode == 500) {
                var msg = res.data.errors.count[0]
                wx.showToast({
                    title: msg,
                    icon: 'none',
                    duration: 1000
                })
            }
 
            if (res.data.status == 0) {
                wx.showToast({
                    title: 'res.data.msg',
                    icon: 'none',
                    duration: 2000
                })
            } else {
                carts[index].count = count;
                that.setData({
                    carts: carts
                });
                that.getTotalPrice();
            }
        })

    },

    /**
     * 绑定减数量事件
     */
    minusCount(e) {
        var that = this
        const index = e.currentTarget.dataset.index;
        const id = e.currentTarget.dataset.id;
        const goods_id = e.currentTarget.dataset.goods_id;
        let carts = that.data.carts;
        let count = carts[index].count;
        if (count <= 1) {
            wx.showToast({
                title: '不能再少了',
                icon: 'none',
                duration: 2000
            })
            return false;
        }
        count = count - 1;
        app._http('cart/' + id + '/update', {
            goods_id:goods_id,
            count: count
        }, 'put', function(res) {
            if (res.data.status == 0) {
                wx.showToast({
                    title: '减少失败！',
                    icon: 'none',
                    duration: 2000
                })
            } else {
                carts[index].count = count;
                that.setData({
                    carts: carts
                });
                that.getTotalPrice();
            }
        })
    },

    /**
     * 计算总价
     */
    getTotalPrice() {
        let carts = this.data.carts; // 获取购物车列表
        console.log(carts)
        let total = 0;
        for (let i = 0; i < carts.length; i++) { // 循环列表得到每个数据
            if (carts[i].selected) { // 判断选中才会计算价格
                total += carts[i].count * carts[i].goods.price; // 所有价格加起来
            }
        }

        this.setData({ // 最后赋值到data中渲染到页面
            carts: carts,
            totalPrice: total.toFixed(2)
        });
        return total.toFixed(2);
    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this
        that.loadCart()
        this.getTotalPrice();
        wx.stopPullDownRefresh();
    }


})