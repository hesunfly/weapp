var app = getApp();
Page({
  data: {
    id: ''
  },
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: app.globalData.title_color,
      backgroundColor: app.globalData.color,
    })
    this.setData({
      id: options.id
    })
  },
  //查看订单
  tapLook: function () {
    wx.navigateTo({ url: 'order_details?orderid=' + this.data.id });

  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})