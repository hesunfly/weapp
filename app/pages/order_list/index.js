var app = getApp()
Page({
  data: {
    statusType: ["全部", "待确认", "待收货", "已完成"],
    currentTpye: 0,
    tabClass: ["", "", "", "", ""],
    orderlist:[],
    attachment:''
  },
  statusTap: function (e) {
    var curType = e.currentTarget.dataset.index;
    this.data.currentTpye = curType
    this.setData({
      currentTpye: curType
    });
    this.onShow();
  },
  orderDetail: function (e) {
    var orderId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/order-details/index?id=" + orderId
    })
  },
  //取消订单
  cancelOrderTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          app.util.request({
            url: 'entry/wxapp/orderlist',
            method: 'GET',
            data: {
              op: 'cancel',
              id: orderId
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {
              if(res.data.errno == 0) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
                that.onShow();
              }
            }
          })
        }
      }
    })
  },
  //删除订单
  deleteOrderTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定要删除该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {

          app.util.request({
            url: 'entry/wxapp/orderlist',
            method: 'GET',
            data: {
              op: 'delete',
              id: orderId
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {

              if (res.data.errno == 0) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
                that.onShow();
              } 
            }
          })
        }
      }
    })
  },
  //确认收货
  entryOrderTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    wx.showModal({
      title: '确定已收到此订单商品吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {

          app.util.request({
            url: 'entry/wxapp/orderlist',
            method: 'GET',
            data: {
              op: 'entry',
              id: orderId
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {
              if (res.data.errno == 0) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 2000
                })
                that.onShow();
              }
            }
          })
        }
      }
    })
  },
  toPayBtn: function (e) {//马上支付
    var orderId = e.currentTarget.dataset.id;
    var money = e.currentTarget.dataset.money; 
    wx.request({
      url: app.util.url('/entry/wxapp/payByOrderId'),
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      method: "POST",
      data: {},
      success: function (res) {
        if (res.data.errno == 0) {
          wx.requestPayment({
            'timeStamp': res.data.data.timeStamp,
            'nonceStr': res.data.data.nonceStr,
            'package': res.data.data.package,
            'signType': 'MD5',
            'paySign': res.data.data.paySign,
            'success': function (res) {
              wx.showToast({ title: '支付成功' })
              wx.reLaunch({
                'url': '../order_list/index'
              }); 
            },
            'fail': function ($data) {
              wx.showToast({
                title: '支付失败',
                icon: 'success',
                duration: 500,
                mask: true
              })
            }
          })
        } else {
          console.log(res);
        }
      
      },
      fail: function (res) {
        if (res.data.errno == 0) {
          wx.requestPayment({
            'timeStamp': res.data.data.timeStamp,
            'nonceStr': res.data.data.nonceStr,
            'package': res.data.data.package,
            'signType': 'MD5',
            'paySign': res.data.data.paySign,
            'success': function (res) {
              console.log(res);
              wx.showToast({ title: '支付成功' })
              wx.reLaunch({
                'url': '../order_list/index'
              });
            },
            'fail': function (res) {
              wx.showToast({
                title: '支付失败',
                icon: 'success',
                duration: 500,
                mask: true
              })
            }
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 500,
            mask: true
          })
        }
      }

    });

  },
  onLoad: function (options) {
    // 生命周期函数--监听页面加载 
    this.initOrder();
  },
  onReady: function () {
    // 生命周期函数--监听页面初次渲染完成 
  },
  onShow: function () {// 获取订单列表 
    // this.initOrder();
  },
  initOrder:function(){
    wx.showLoading();
    var that = this;

    // if (that.data.currentTpye == 1) {//待确认
    //   status = 1;
    // }
    // if (that.data.currentTpye == 2) {//待收货
    //   status = 2;
    // }
    // if (that.data.currentTpye == 3) {//已完成
    //   status = 3;
    // }
      app.util.request({
        url: 'entry/wxapp/orderlist',
        data: postData,
        header: {
          'Accept': 'application/json'
        },
      success: function (res) {
        // console.log(res.data.data);
        if (res.data.errno == 0) {
          var orderlist = res.data.data.orderlist;
          that.setData({
            orderList: orderlist,
            attachment:app.globalData.attachment
          })
        } else {
          that.setData({
            orderList: null
          });
        }
      },
      fail: function (res) {
        if (res.data.errno == 0) {
          that.setData({
            orderList: res.data.data
          })
        } else {
          that.setData({
            orderList: null
          });
        }
      },
      complete: function () {
        // console.log('数据')
        wx.hideToast();
      }
    }); 
  },
  onHide: function () {
  },
  onUnload: function () {
  },

  onReachBottom: function () {
  },
   //下拉刷新
  onPullDownRefresh: function () {
    this.initOrder();
    wx.stopPullDownRefresh()
  }
})