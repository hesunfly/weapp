var server = require('../../resource/utils/server.js');
var app = getApp();
Page({
  data: { 
    shopId: 0,
    _shop_info: '', 
    glo_is_load: false
  },
  diancan_formsubmit: function (e) {
    var that = this;
    var form_id = e.detail.formId;
    that.insertFormID(form_id); 
    if (that.data._shop_info.dish_is_diannei == 1) {
      if (that.data._shop_info.dish_is_rcode_open == 1) {
        wx.scanCode({
          success: (res) => {
            if (res.path) {
              wx.navigateTo({
                url: '/' + res.path
              });
            }
          }
        });
      } else {
        wx.navigateTo({
          url: '../shop?id=' + that.data.shopId + '&order_type=1'
        });
      }
    } else {
      wx.showModal({
        title: '提示',
        content: "对不起，暂不支持店内点餐",
        showCancel: false
      });
      return;
    }
  },
  go_dish_index_bind: function () {
    wx.switchTab({
      url: '/pages/restaurant/restaurant-home/index'
    })
  },
  //预订
  dish_yuding_bind: function (e) {
    var that = this;
    if (that.data.this_dish_info.dish_is_yuding == 1) {
      wx.navigateTo({
        url: '../restaurant-reserve/index?dish_id=' + e.currentTarget.id
      });
    } else {
      wx.showModal({
        title: '提示',
        content: "对不起，暂不支持预定",
        showCancel: false
      });
      return;
    }
  }, 
  paidui_formsubmit: function (e) { //排队
    var that = this;
    var form_id = e.detail.formId;
    that.insertFormID(form_id);
    if (that.data.this_dish_info.dish_is_paidui == 1) {
      wx.navigateTo({
        url: '../paidui/index?dish_id=' + e.detail.target.dataset.id
      });
    } else {
      wx.showModal({
        title: '提示',
        content: "对不起，暂不支持排队",
        showCancel: false
      });
      return;
    }
  },
  insertFormID: function (form_id) {
    var that = this;
    requestUtil.get(_DuoguanData.duoguan_host_api_url + '/index.php?s=/addon/DuoguanUser/Api/addUserFormId.html', { form_id: form_id }, (info) => {

    }, that, { isShowLoading: false });
  },
  //外卖
  dish_waimai_bind: function (e) {
    var that = this;
    if (that.data._shop_info.dish_is_waimai == 1) {
      wx.navigateTo({
        url: '../restaurant-single/index?dish_id=' + e.currentTarget.id + '&order_type=2'
      });
    } else {
      wx.showModal({
        title: '提示',
        content: "对不起，暂不支持外卖",
        showCancel: false
      });
      return;
    }
  },
  //转账
  zhuanzhang_bind: function (e) {
    wx.navigateTo({
      url: '../pay/index?dish_id=' + e.currentTarget.id
    });
  },
  //导航
  get_location_bind: function () {
    wx.showToast({
      title: '地图加载中',
      icon: 'loading',
      duration: 10000,
      mask: true
    });
    var that = this;
    var loc_lat = that.data._shop_info.lat;
    var loc_lng = that.data._shop_info.lng;
    wx.openLocation({
      latitude: parseFloat(loc_lat),
      longitude: parseFloat(loc_lng),
      scale: 18,
      name: that.data._shop_info.dish_name,
      address: that.data._shop_info.dish_address
    });
  },
  //电话
  call_phone_bind: function () {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: that.data._shop_info.dish_con_mobile
    });
  },
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: app.globalData.title_color,
      backgroundColor: app.globalData.color,
    })
    var that = this;
    var shopid = options.id;
    var selectShop = server.selectedShopDetail(shopid)
    that.setData({
      shopId: shopid,
      text: selectShop.shop_notice,
      _shop_info: selectShop
    })
    wx.setNavigationBarTitle({
      title: selectShop.title
    })  
  },
  onShow: function () {
    wx.hideToast(); 
  }, 
  //图片放大
  img_max_bind: function (e) {
    var that = this;
    wx.previewImage({ current: e.target.dataset.url, urls: that.data.this_dish_info.dish_shijing_arr });
  },
  img_max_bind_zz: function (e) {
    var that = this;
    wx.previewImage({ current: e.target.dataset.url, urls: that.data.this_dish_info.dish_zizhi_arr });
  },
  //下拉刷新
  onPullDownRefresh: function () {
    var that = this;
    _function.dishGetDishConfig(that.initdishGetDishConfigData, that);
    setTimeout(() => {
      wx.stopPullDownRefresh()
    }, 1000);
  },
  onShareAppMessage: function () {
    var that = this;
    var shareTitle = that.data.this_dish_info.dish_name;
    var shareDesc = that.data.this_dish_info.dish_jieshao;
    var sharePath = 'pages/restaurant/restaurant-home-info/index?d_type=single&dish_id=' + that.data.this_dish_id;
    return {
      title: shareTitle,
      desc: shareDesc,
      path: sharePath
    }
  },
})