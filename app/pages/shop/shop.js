var app = getApp();
var server = require('../../resource/utils/server.js');
Page({
  data: {
    cart: {
      count: 0,
      goodsid: 0,
      total: 0
    },
    attachment:'',
    cartList: [],
    localList: [],
    shopId: 0,
    shop: [],
    text: '',
    disabled:0,
    desc:'',
    hasNoCoupons: true,
    coupons: [],
    marqueePace: 1,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marqueeDistance2: 0,
    marquee2copy_status: false,
    marquee2_margin: 60,
    size: 14,
    classify:[],
    orientation: 'left',//滚动方向
    interval: 20,// 时间间隔
    showCartDetail: false,

  },
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: app.globalData.title_color,
      backgroundColor: app.globalData.color,
    })
        var that = this
        var shopid = options.id;

        var selectShop = server.selectedShopDetail(shopid)
        // console.log(selectShop)
        that.setData({
          shopId: shopid,
          text: selectShop.notice,
          shop: selectShop,
          attachment:app.globalData.attachment
        })
        wx.setNavigationBarTitle({
          title: selectShop.title
        })
        that.initGoods();

      /** var length = that.data.text.length * that.data.size;//文字长度
        var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
        that.setData({
          length: length,
          windowWidth: windowWidth,
          marquee2_margin: length < windowWidth ? windowWidth - length : that.data.marquee2_margin

        });**/
       // that.run1();// 水平一行字滚动完了再按照原来的方向滚动
       // that.run2();// 第一个字消失后立即从右边出现
        // that.getCoupons(shopid);
        // if (selectShop.isstop==0){
        //   wx.showModal({
        //     title: '提示',
        //     content: '商家暂停接单',
        //     showCancel: true,
        //     success: function (res) {
        //       if (res.confirm) {
        //       }
        //     }
        //   })
        //   return false;
        // }
  },

  initGoods:function(){
    var that=this;
    //获取商品信息
      app.util.request({
        url: 'entry/wxapp/showgoods',
        data: {
          'op': 'display', 'storeid': that.data.shopId
        },
        header: {
          'Accept': 'application/json'
        },
      success: function (res) {
        if (res.data.errno == 0) {
          // console.log(res.data.data);
          // 获取商品数据
          that.setData({
            classify: res.data.data,
          })
        }

      }
    })
    var res = wx.getStorageSync('orderList_' + that.data.shopId);
    // console.log('order', res);
    if (res) {
      that.setData({
        cart: {
          count: res.count,
          total: res.total
        }
      });
      if (!server.isEmptyObject(res.cartList)) {
        that.setData({
          cartList: res.cartList,
          localList: server.filterEmptyObject(res.cartList)
        })
      }
    }
    if (typeof that.data.cartList[that.data.shopId] == 'undefined' || server.isEmptyObject(that.data.cartList[that.data.shopId])) {
      var cartList = that.data.cartList;
      cartList[that.data.shopId] = [];
      that.setData({
        cartList: cartList
      })
    }
    // console.log(app.globalData.PersonInfo)

    // var key = 'followed_' + shopid + "_" + app.globalData.userInfo.memberInfo.uid;
    // var res = wx.getStorageSync(key);
    // console.log(key)
    if (res) {
      that.setData({
        followed: res.followed
      });
    }

    that.setData({
      desc: that.cashDesc()
    })
  },
  onShow: function () {
    // console.log(this.data.classify);
    var that=this;
    this.setData({
      classifySeleted: 1
    });

  },
  checkOrderSame: function (name) {
    var list = this.data.cartList[this.data.shopId];
    for (var index in list) {
      if (list[index].name === name) {
        return index;
      }
    }
    return false;
  },
  tapAddCart: function (e) {
    var that=this;
    console.log(e)
    var price = parseFloat(e.target.dataset.price);
    var name = e.target.dataset.name;
    var goodsid = e.target.dataset.goodsid;
    var img = e.target.dataset.pic;
    var is_rest = e.target.dataset.is_rest;
    var is_delivery = e.target.dataset.is_delivery;
    var key = e.target.dataset.key;
    var list = this.data.cartList;
    var sortedList = [];
    var shop = this.data.shop
    // var goodlist = shop.menu[key].goods
    console.log(price);


    console.log('加入')

    // 店铺是否打样  is_rest
    if (is_rest==false){
      wx.showModal({
        title: '提示',
        content: '商家暂停接单',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
          }
        }
      })
      return false;
    }
    // 店铺是否在配送时间范围内 is_delivery
    if (is_delivery == false) {
      wx.showModal({
        title: '提示',
        content: '不在配送范围内',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
          }
        }
      })
      return false;
    }
  // console.log(that.data.shop.id);
  // console.log(that.shop);
    var index;
    if (index = this.checkOrderSame(name)) {
      sortedList = list[this.data.shopId][index];
      var num = list[this.data.shopId][index].num;
      list[this.data.shopId][index].num = num + 1;

    }
    else {
      var order = {
        "price": price,
        "num": 1,
        "name": name,
        'img':img,
        "goodsid": goodsid,
        "shopId": that.data.shopId,
        "shopName": that.data.shop.title,
        "pay": 0,
      }
      list[that.data.shopId].push(order);
      sortedList = order;
    }


    // goodlist.forEach(function (good, kk) {
    //   if (good.id == goodsid) {
    //     shop.menu[key].goods[kk].isadd = 1
    //   }
    //   console.log(list)

    //   list[that.data.shopId].forEach(function(lis,k){

    //   if(lis != null && lis.goodsid==goodsid){
    //     shop.menu[key].goods[kk].num = lis.num

    //   }
    //   })
    // })

    this.setData({
      cartList: list,
      localList: server.filterEmptyObject(list),
      shop: shop

    });
    this.addCount(sortedList);
    // console.log(this.data.shop)
    // console.log(list)


  },
  tapReduceCart: function (e) {
    // console.log(e);
    var name = e.target.dataset.name;
    var price = parseFloat(e.target.dataset.price);
    var list = this.data.cartList;
    var index, sortedList = [];
    var num = e.target.dataset.num;
    var key = e.target.dataset.key;
    var goodsid = e.target.dataset.goodsid;
    var shop = this.data.shop
    // var goodlist = shop.menu[key].goods
    if (index = this.checkOrderSame(name)) {
      var num = list[this.data.shopId][index].num
      // console.log(num)
      if (num >= 1) {
        sortedList = list[this.data.shopId][index];
        list[this.data.shopId][index].num = num - 1;
      }else {
        sortedList = list[this.data.shopId][index]
        list[this.data.shopId].splice(index, 1);
      }
    }

    var that = this
    // goodlist.forEach(function (good, kk) {
    //   if (good.id == goodsid) {
    //     shop.menu[key].goods[kk].isadd = 1
    //   }
    //   list[that.data.shopId].forEach(function (lis, k) {
    //     if (lis != null && lis.goodsid == goodsid) {
    //       shop.menu[key].goods[kk].num = lis.num
    //     }
    //   })
    // })
    // console.log(list)
    var len=list.length;
    var goodslist=[]
    for(var i=0;i<len;i++){
      if(list[i]){
        var lenn=list[i].length;
        for(var j=0;j<lenn;j++){
          // console.log(list[i][j].num)
          if(list[i][j].num > 0){
              goodslist[j]=list[i][j]
          }
        }
      }
    }

    this.setData({
      cartList: list,
      localList: server.filterEmptyObject(list),
      shop: shop

    });

    this.deduceCount(sortedList,num);
  },
  addCount: function (list) {

    var count = this.data.cart.count + 1,
      total = (parseInt(this.data.cart.total * 1000) + parseInt(list.price*1000)) / 1000;
    console.log(parseInt(this.data.cart.total * 1000)/1000)
    console.log(parseInt(list.price * 1000) / 1000)
      console.log(total);
    this.saveCart(count, total);
  },
  deduceCount: function (list,num) {
    // var count = 0,total = 0;
    // if (this.data.cart.count>=1){
      var count = this.data.cart.count - 1,
        total = (parseInt(this.data.cart.total * 100) - parseInt(list.price * 100)) / 100;
    // }
      // console.log(count)
    if(num>0){
      this.saveCart(count, total);

    }

  },
  saveCart: function (count, total) {
    total = parseFloat(total);
    if (typeof total == null)
      total = 0;
    this.setData({
      cart: {
        count: count,
        total: total
      }
    });
    this.setData({
      desc: this.cashDesc()
    })
    // console.log(this.data.shopId)

    wx.setStorage({
      key: 'orderList_' + this.data.shopId,
      data: {
        cartList: this.data.cartList,
        count: this.data.cart.count,
        total: this.data.cart.total,
      }
    })
  },
  // onGoodsScroll: function (e) {
  //   console.log(e)
  //   if (e.detail.scrollTop > 10 && !this.data.scrollDown) {
  //     this.setData({
  //       scrollDown: true
  //     });
  //   } else if (e.detail.scrollTop < 10 && this.data.scrollDown) {
  //     this.setData({
  //       scrollDown: false
  //     });
  //   }
  //   // console.log(this.data.classify);
  //   var scale = e.detail.scrollWidth / 570,
  //     scrollTop = e.detail.scrollTop / scale,
  //     h = 0,
  //     classifySeleted;
  //     var length=0;
  //     for (var ever in this.data.classify.cate) {
  //       length++;
  //     }
  //     var len=length;
  //     // len = this.data.classify.cate.length;
  //     // console.log(len);
  //   this.data.shop.cate.forEach(function (classify, i) {

  //     var _h = 70 + classify.goods.length * (46 * 3 + 20 * 2);
  //     if (scrollTop >= h - 100 / scale) {
  //       classifySeleted = classify.id;
  //     }
  //     h += _h;
  //   });

  //   this.setData({
  //     classifySeleted: classifySeleted
  //   });
  // },
  tapClassify: function (e) {
    console.log(e)
    var id = e.target.dataset.id;
    var shopId = e.target.dataset.shopid;
    this.setData({
      classifyViewed: id
    });
    var self = this;
    setTimeout(function () {
      self.setData({
        classifySeleted: id
      });
    }, 100);
    app.util.request({
      url: 'entry/wxapp/showgoods',
      data: {
        'op': 'getCate', 'storeid': shopId,'cate_id':id
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.data.errno == 0) {
          // console.log(res.data.data);
          // 获取商品数据
          self.setData({
            classify: res.data.data,
          })
        }

      }
    })
  },
  showCartDetail: function () {
    this.setData({
      showCartDetail: !this.data.showCartDetail
    });
  },
  hideCartDetail: function () {
    this.setData({
      showCartDetail: false
    });
  },
  submit: function (e) {
    var total = this.data.cart.total
    wx.navigateTo({
      url: '/app/pages/to-pay-order/pay_order?pay=1&total=' + total + '&shopid=' + this.data.shopId
    })
  },
  onShareAppMessage: function () {
    return {
      title: this.data.shop.title,
      path: 'app/pages/shop/shop?id=' + this.data.shopid,
      success: function (res) {
        wx.showToast({
          title: '转发成功',
          icon: 'success',
          duration: 1000,
          mask: true
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
          icon: 'error',
          duration: 1000,
          mask: true
        })
      }
    }
  },
  run1: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance < vm.data.length) {
        vm.setData({
          marqueeDistance: vm.data.marqueeDistance - vm.data.marqueePace,
        });
      } else {
        clearInterval(interval);
        vm.setData({
          marqueeDistance: vm.data.windowWidth
        });
        vm.run1();
      }
    }, vm.data.interval);
  },
  run2: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance2 < vm.data.length) {// 如果文字滚动到出现marquee2_margin=30px的白边，就接着显示
        vm.setData({
          marqueeDistance2: vm.data.marqueeDistance2 - vm.data.marqueePace,
          marquee2copy_status: vm.data.length + vm.data.marqueeDistance2 <= vm.data.windowWidth + vm.data.marquee2_margin,
        });
      } else {
        if (-vm.data.marqueeDistance2 >= vm.data.marquee2_margin) { // 当第二条文字滚动到最左边时
          vm.setData({
            marqueeDistance2: vm.data.marquee2_margin // 直接重新滚动
          });
          clearInterval(interval);
          vm.run2();
        } else {
          clearInterval(interval);
          vm.setData({
            marqueeDistance2: -vm.data.windowWidth
          });
          vm.run2();
        }
      }
    }, vm.data.interval);
  },
  openwin: function (event) { //跳转页面
    var path = event.target.dataset.url;
    wx.navigateTo({
      url: '../' + path + '/' + path + '?shopid=' + event.target.dataset.shopid
    })
  },
  //获取优惠券信息
  getCoupons: function (shopid) {
    var that = this;
    console.log('获取优惠券信息')
    wx.request({
      url: app.util.url(''),
      method: 'GET',
      data: { id: shopid},
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        // console.log(res.data.data.coupons)
        if (res.data.code == 0) {
          that.setData({
            hasNoCoupons: false,
            // coupons: res.data.data.coupons
          });
        }
      }
    })
  },
  gitCoupon: function (e) {
    var that = this;

    wx.request({
      url: app.util.url('/entry/wxapp/ApiDrawCoupons/'),
      method: 'GET',
      data: {
         shopid: that.data.shopId,
        couponID: e.currentTarget.dataset.id
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.data.errno == 20003) {
          wx.showModal({
            title: '错误',
            content: '你领过了，别贪心哦~',
            showCancel: false
          })
          return;
        }
        if (res.data.errno == 30001) {
          wx.showModal({
            title: '错误',
            content: '您的积分不足',
            showCancel: false
          })
          return;
        }
        if (res.data.errno == 20004) {
          wx.showModal({
            title: '错误',
            content: '已过期~',
            showCancel: false
          })
          return;
        }
        if (res.data.errno == 0) {
          wx.showToast({
            title: '领取成功，赶紧去下单吧~',
            icon: 'success',
            duration: 2000
          })
        } else {
          wx.showModal({
            title: '错误',
            content: res.data.message,
            showCancel: false
          })
        }

      }
    })


  },
  cashDesc() {
    if (this.data.cart.total == 0) {
      return '￥' + this.data.shop.content + '元起送';
    } else if (this.data.cart.total <= 0) {
      return '￥' + this.data.shop.content + '元起送';
    } else if (this.data.cart.total < this.data.shop.content) {
      let diff = this.data.shop.content - this.data.cart.total ;
      return '还差' + diff + '元起送';
    } else {
      this.setData({
        disabled: 1
      });
      return '去结算';
    }
  }
});

