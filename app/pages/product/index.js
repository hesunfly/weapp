
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ui:'',
        attachment:'',
        product:[],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        app.tools.commonSet(app)
        that.setData({
            resource: app.globalData.resource
        })
       
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        var that = this
        that.getUi()
        that.getProduct()
    },

    getUi:function(){
        var that = this;

        app.http('product/ui', '', 'get', function (res) {
            that.setData({
                ui: res.data.data
            })
        })
    },
    getProduct: function () {
        var that = this;

        app.http('product/index', '', 'get', function (res) {
            that.setData({
                product: res.data.data
            })
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})