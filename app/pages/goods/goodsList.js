var app = getApp()
Page({
    data: {
        currentTab: 0,
        goods:[],
        currentTab: 0,
        imageurl1: "/app/resource/imgs/shangHui.png",
        imageurl2: "/app/resource/imgs/xiaHei.png",
        daindex2: 0,

        imageurl3: "/app/resource/imgs/shangHui.png",
        imageurl4: "/app/resource/imgs/xiaHei.png",
        daindex1: 0,

        imageurl5: "/app/resource/imgs/shangHui.png",
        imageurl6: "/app/resource/imgs/xiaHei.png",
        daindex3: 0,
    },
    onLoad(e) {
        let id = e.id;
        let title = e.title;
        var that = this;
        app.tools.commonSet(app)
        that.setData({
            resource: app.globalData.resource,
        })
        wx.setNavigationBarTitle({
            title: title
        })
        that.loadGoods(id)
    },
    loadGoods:function(id){
        var that = this;
        app.http('tag/' + id + '/list', '', 'get', function (res) {
            console.log(res.data.data)
            that.setData({
                goods: res.data.data
            })
        })
    },
    // clickTab: function(e) {
    //     var that = this;
    //     if (this.data.currentTab === e.target.dataset.current) {
    //         return false;
    //     } else {
    //         that.setData({
    //             currentTab: e.target.dataset.current
    //         })
    //     }
    // },
    choosesort1: function (e) {
        if (this.data.daindex2 == 0) {
            this.setData({
                imageurl2: "/app/resource/imgs/xiaHui.png",
                imageurl1: "/app/resource/imgs/shangHei.png",
                daindex2: 1
            })
        } else {
            this.setData({
                imageurl2: "/app/resource/imgs/xiaHei.png",
                imageurl1: "/app/resource/imgs/shangHui.png",
                daindex2: 0
            })
        }

    },
    choosesort2: function (e) {
        console.log(this.data.daindex1)
        if (this.data.daindex1 == 0) {
            this.setData({
                imageurl4: "/app/resource/imgs/xiaHui.png",
                imageurl3: "/app/resource/imgs/shangHei.png",
                daindex1: 1
            })
        } else {
            this.setData({
                imageurl4: "/app/resource/imgs/xiaHei.png",
                imageurl3: "/app/resource/imgs/shangHui.png",
                daindex1: 0
            })
        }

    },
    choosesort3: function (e) {
        if (this.data.daindex3 == 0) {
            this.setData({
                imageurl6: "/app/resource/imgs/xiaHui.png",
                imageurl5: "/app/resource/imgs/shangHei.png",
                daindex3: 1
            })
        } else {
            this.setData({
                imageurl6: "/app/resource/imgs/xiaHei.png",
                imageurl5: "/app/resource/imgs/shangHui.png",
                daindex3: 0
            })
        }

    },
    clickTab: function (e) {
        let clickFlag = e.target.dataset.current;
        if (clickFlag == 1) {
            this.choosesort1()
        }
        else if (clickFlag == 2) {
            this.choosesort2()
        }
        else if (clickFlag == 3) {
            this.choosesort3()
        }
        else {

        }
        var that = this;
        if (this.data.currentTab === clickFlag) {
            return false;
        } else {
            that.setData({
                currentTab: clickFlag
            })
        }
    }  

})