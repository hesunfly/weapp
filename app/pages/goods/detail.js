var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        detail: [],
        resource: '',
    },
    onLoad: function(e) {
        var that = this
        var id = e.id;
        that.setData({
            resource: app.globalData.resource
        })
        app.tools.commonSet(app)
        
        that.getGoods(id)
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    getGoods: function(id) {
        var that = this
        app._http('goods/show/' + id, '', 'get',function(res) {
            console.log(res.data.images);
            that.setData({
                detail: res.data,
            })
            console.log(that.data.detail)
        })


    },
    collectionFun(e){
        var goods_id = e.target.dataset.id
        console.log(goods_id)
        app._http('collection/store', {"goods_id":goods_id}, 'post', function (res) {
            if (res.status == 1){
                if (that.data.detail.is_collection == 0){
                    that.data.detail.is_collection = 1;
                }
                else{
                    that.data.detail.is_collection = 0;
                }
                that.setData({
                    detail: that.data.detail
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },



    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */


    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})