var app = getApp();

Page({
    data: {
        copyright: '',
        tel: '',
        time: '',
        userInfo: [],
      
    },
    onLoad: function() {
        app.tools.commonSet(app)
        var that = this 
        that.getSysinfo();
        
        // 获取缓存信息
    },
    onShow: function() {
        var that = this
        
        app.checkToken()
        that.getAuth()

    },
    getAuth: function () {
        wx.getSetting({
            success(res) {
                if (!res.authSetting['scope.userInfo']) {
                    wx.navigateTo({
                        url: '/app/pages/to_login/to_login'
                    })
                }
            }
        })
    },
    getSysinfo: function() {
        //获取平台信息
        var that = this;
        app.http('setting/service', '', 'get', function (res) {
            that.setData({
                tel: res.data.tel,
                time: res.data.time,
            });
            console.log(that.data.tel);
        })
    },
    bindAddress: function() {
        var that = this
        wx.navigateTo({
            url: '/app/pages/address/index'
        })
        /**if (wx.chooseAddress) {
          wx.chooseAddress({
            success: function (res) {
              var address = {
                userName: res.userName, //姓名
                telNumber: res.telNumber, //手机号
                provinceName: res.provinceName, //省份
                postalCode: res.postalCode,//邮编
                nationalCode: res.nationalCode,//国家
                cityName: res.cityName, //市
                countyName: res.countyName, //区
                detailInfo: res.detailInfo,   //详细地址
              };
              that.setData({
                address: address
              })
              console.log(address)
            },
            fail: function (res) {
              console.log('获取地址失败')
            },
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
          })
        } **/
    },
    setPhoneCall: function() {
        var that = this;
        wx.makePhoneCall({
            phoneNumber: that.data.tel
        })
    },
    //下拉刷新
    onPullDownRefresh: function() {
        var that = this
        that.getSysinfo();
        this.setData({
            userInfo: app.globalData.userInfo,
            copyright: app.globalData.copyright
        });
        wx.stopPullDownRefresh()
    }
});