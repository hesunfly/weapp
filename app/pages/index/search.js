var app = getApp();
// console.log(app.globalData)
// var server = require('../../utils/server');
Page({
    data: {
        filterId: 1,
        searchWords: '',
        placeholder: '请输入关键字',
        attachment: '',

    },
    onLoad: function() {
        this.setData({
            attachment: app.globalData.attachment
        });

        wx.setNavigationBarColor({
            frontColor: app.globalData.title_color,
            backgroundColor: app.globalData.color,
        })
    },
    onShow: function() {
        this.setData({
            showResult: false
        });
    },
    inputSearch: function(e) {
        this.setData({
            searchWords: e.detail.value
        });
    },
    doSearch: function() {
        var that = this;
        var keyword = that.data.searchWords;
        if(!keyword){
            wx.showToast({
                title: '请输入关键字',
                icon: 'none',
                duration: 2000
            })
        }

        app.http('search',{keyword:keyword},'get',function(res){
            
        });

    },
    tapFilter: function(e) {
        var that = this;
        switch (e.target.dataset.id) {
            case '1':
                that.data.shop.sort(function(a, b) {
                    return a.id - b.id;
                });
                break;
            case '2':
                that.data.shop.sort(function(a, b) {
                    return b.sales - a.sales;
                });
                break;
            case '3':
                that.data.shop.sort(function(a, b) {
                    return a.dis - b.dis;
                });
                break;
        }
        that.setData({
            filterId: e.target.dataset.id,
            shop: that.data.shop
        });
    }
});