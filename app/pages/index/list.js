var app = getApp();
 Page({ 
  data: {
    filterId: 1,
  }, 
  onLoad: function (options) {
    let that = this
    let id = options.id;
    wx.setNavigationBarTitle({
      title: options.title,
    })
    wx.setNavigationBarColor({
      frontColor: app.globalData.title_color,
      backgroundColor: app.globalData.color,
    }) 
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        var latitude = res.latitude;
        var longitude = res.longitude;

          app.util.request({
            url: 'entry/wxapp/showstores',
            data: {
              'op': 'shoptag',
              'label': id, 
              'lat': latitude, 
              'lng': longitude
            },
            header: {
              'Accept': 'application/json'
            },
          success: function (res) {
          if (res.data.errno == 0) {
            // console.log(res.data.data);
            //获取商品数据
              that.setData({
                shops: res.data.data.list,
                attachment: app.globalData.attachment,
              })
              app.globalData.shops = res.data.data.list;
            }
            
          }
        })
      }
    });  
  }, 
  onReady: function () { 
  }, 
  tapFilter: function (e) {
    var that=this;
    switch (e.target.dataset.id) {
      case '1':
        that.data.shops.sort(function (a, b) {
          return a.id - b.id;
        });
        break;
      case '2':
        that.data.shops.sort(function (a, b) {
          return b.sales - a.sales;
        });
        break;
      case '3':
        that.data.shops.sort(function (a, b) {
          return a.dis - b.dis;
        });
        break;
    }
    this.setData({
      filterId: e.target.dataset.id,
      shops: that.data.shops
    });
  },
  onShow: function () {
  
  }, 
  onHide: function () {
  
  }, 
  onUnload: function () {
  
  }, 
  onPullDownRefresh: function () {
  
  }, 
  onReachBottom: function () {
  
  }, 
  onShareAppMessage: function () {
  
  }, 
  onReachBottom: function () {
  
  }, 
  onPullDownRefresh: function () {
  
  }
})