var app = getApp();

var QQMapWX = require('../../resource/utils/qqmap-wx-jssdk.js');

Page({
    data: {
        pageItems: [],
        filterId: 1,
        carousel: [],
        tag: [],
        floor: [],
        resource: '',
        address: '',

    },
    onLoad: function() {
        var that = this;
        that.setData({
            resource: app.globalData.resource
        })
        app.tools.getSetting(app)

        that.getTag()
        that.getCarousel()
        that.getFloor()

    },
    onShow: function() {
        var that = this
        app.checkToken()
        that.initAddress()
    },
    initAddress: function() {
        var that = this;
        var qqmapsdk = new QQMapWX({
            key: 'QFKBZ-3TBH6-T4ZSX-MCPC3-J7OX2-WQFEG' // 必填
        });
        //1、获取当前位置坐标
        wx.getLocation({
            type: 'gcj02',
            success: function(res) {
                //2、根据坐标获取当前位置名称，显示在顶部:腾讯地图逆地址解析
                qqmapsdk.reverseGeocoder({
                    success: function(addressRes) {
                        var address = addressRes.result.formatted_addresses.recommend;
                        app.globalData.latitude = addressRes.result.location.lat;
                        app.globalData.longitude = addressRes.result.location.lng;
                        app.globalData.city = addressRes.result.address_component.city;
                        that.setData({
                            address: address,
                        })
                    }
                })
            }
        });
    },
    getTag: function() {
        var that = this
        app.http('tag', '', 'get', function(res) {

            that.setData({
                tag: res.data.data
            })

        });
    },
    getCarousel: function() {
        var that = this
        app.http('carousel', '', 'get', function(res) {
            that.setData({
                carousel: res.data.data
            })
        });
    },
    getFloor: function() {
        var that = this
        app.http('floor', '', 'get', function(res) {
            that.setData({
                floor: res.data.data
            })
    
        })
    },
    addCart: function(e) {
        var that = this
        var goods_id = e.currentTarget.dataset.id
        app._http('cart/create', {
            goods_id: goods_id,
            count: 1
        }, 'post', function(res) {
            if (res.data.status == 1) {
                wx.showToast({
                    title: '添加成功',
                    icon: 'success',
                    duration: 1000
                })
            } else {
                wx.showToast({
                    title: '添加失败',
                    icon: 'none',
                    duration: 2000
                })
            }
        })
    },

    onScroll: function(e) {
        if (e.detail.scrollTop > 100 && !this.data.scrollDown) {
            this.setData({
                scrollDown: true
            });
        } else if (e.detail.scrollTop < 100 && this.data.scrollDown) {
            this.setData({
                scrollDown: false
            });
        }
    },
    tapSearch: function() {
        wx.navigateTo({
            url: 'search'
        });
    },
    navigatorList: function(e) {
        var id = e.target.dataset.id;
        wx.navigateTo({
            url: 'list?categoryid=' + id
        });
    },

    tapFilter: function(e) {
        var that = this;
        switch (e.target.dataset.id) {
            case '1':
                that.data.store_tj.sort(function(a, b) {
                    return a.id - b.id;
                });
                break;
            case '2':
                that.data.store_tj.sort(function(a, b) {
                    return b.sales - a.sales;
                });
                break;
            case '3':
                that.data.store_tj.sort(function(a, b) {
                    return a.dis - b.dis;
                });
                break;
        }
        that.setData({
            filterId: e.target.dataset.id,
            store_tj: that.data.store_tj
        });
    },
    doNav: function(e) {
        wx.openLocation({
            name: e.target.dataset.name,
            address: e.target.dataset.address,
            latitude: e.target.dataset.latitude,
            longitude: e.target.dataset.longitude,
            scale: 28,
            success: function(res) {}
        })
    },
    tapBanner: function(e) {
        var name = this.data.banners[e.target.dataset.id].name;
        // wx.showModal({
        //   title: '提示',
        //   content: '您点击了“' + name + '”活动链接！',
        //   showCancel: false
        // });
    },
    onShareAppMessage: function() {
        return {
            title: '外卖点餐+',
            path: 'app/pages/index/index',
            success: function(res) {
                wx.showToast({
                    title: '转发成功',
                    icon: 'success',
                    duration: 1000,
                    mask: true
                })
            },
            fail: function(res) {
                wx.showToast({
                    title: '转发失败',
                    icon: 'error',
                    duration: 1000,
                    mask: true
                })
            }
        }
    },
    getAuth: function() {
        wx.getSetting({
            success(res) {
                if (!res.authSetting['scope.userInfo']) {
                    wx.navigateTo({
                        url: '/app/pages/to_login/to_login'
                    })
                } else {
                    app.login();
                }
            }
        })
    },
    onReady: function() {
        const that = this;

    },

    //下拉刷新
    onPullDownRefresh: function() {
        var that = this
        that.initAddress()
        that.getTag()
        that.getCarousel()
        that.getFloor()
        wx.stopPullDownRefresh()
        console.log(11111)
    }
});