var app = getApp()
Page({
  data: {
    id: 0,
    shopid: 0,
    name:''
  },
  onLoad: function (e) {
    var shopid = e.shopid; //
    var that = this;
    var id = e.id; //
    var name = e.name; //
    that.setData({
      shopid: shopid,
      id: id, name: name
    })
  },
  bindCancel: function () {
    wx.navigateBack({})
  },
  bindSave: function (e) {
    var that = this;
    var name = e.detail.value.name;
    if (name == "") {
      wx.showModal({
        title: '提示',
        content: '请填写名称',
        showCancel: false
      })
      return
    }
    var apiAddoRuPDATE = "add";
    var apiAddid = that.data.id;
    if (apiAddid) {
      apiAddoRuPDATE = "update";
    } else {
      apiAddid = 0;
    }

    wx.request({
      url: app.util.url('/entry/wxapp/apiSaveCategory/'),
      method: 'GET',
      data: {
        m: 'app', cid: apiAddid,
        shopid: that.data.shopid ,
        name: name
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
      if (res.data.errno != 0) {
          wx.hideLoading();
          wx.showModal({
            title: '失败',
            content: res.data.message,
            showCancel: false
          })
          return;
        }else{
          wx.showModal({
            title: '提示',
            content: '提交成功'  ,showCancel: false
          })
          wx.navigateBack({})
        }

      }
    })


  },
  deleteCategory: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗,删除分类会删除分类下面的商品？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.util.url('/entry/wxapp/ApiDelCategory/'),
            method: 'GET',
            data: {
              m: 'app', cid: id
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {
             if (retus.data.errno == 0) {
                wx.hideLoading();
                wx.navigateBack({})
              }

            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }
})
