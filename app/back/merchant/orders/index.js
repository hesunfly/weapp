var app = getApp()
Page({
  data: {
    statusType: ["全部", "待付款", "已付款", "已完成"],
    currentTpye: 0,
    shopid:0,
    tabClass: ["", "", "", "", ""]
  },
  statusTap: function (e) {
    var curType = e.currentTarget.dataset.index;
    this.data.currentTpye = curType
    this.setData({
      currentTpye: curType
    });
    this.onShow();
  },
  cancelOrderTap: function (e) {
    var that = this;
    var orderId = e.currentTarget.dataset.id;
    var isdelete = e.currentTarget.dataset.isdelete;
    var txt = isdelete == 1 ? "删除" :"取消";
    wx.showModal({
      title: '确定要' + txt+'该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          wx.showLoading();
          wx.request({
            url: app.util.url('/entry/wxapp/delteOrder/'),
            method: 'GET',
            data: {
              m: 'app',
              orderId: orderId, isdelete: isdelete
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {
              wx.hideLoading();
              if (res.data.errno == 0) {
                that.onShow();
                // wx.removeStorage({
                //   key: 'orderList_'+res.data.data,
                //   success: function (res) {
                //     console.log(res.data)
                //   }
                // })
              } else {
                wx.showToast({
                  title: res.data.message,
                  icon: 'success',
                  duration: 1000,
                  mask: true
                })
              }

            }
          })
        }
      }
    })
  },
  onLoad: function (options) {
    let shopid = options.shopid;
    this.setData({
      shopid: shopid
    })
  },
  onReady: function () {
    // 生命周期函数--监听页面初次渲染完成
  },
  onShow: function () {// 获取订单列表
    wx.showLoading();
    var that = this;
    var postData = { 'm': 'app', 'shopid': that.data.shopid };
    if (that.data.currentTpye == 1) {//待付款
      postData.status = 0
    }
    if (that.data.currentTpye == 2) {//已付款
      postData.status = 1
    }
    if (that.data.currentTpye == 3) {//已完成
      postData.status = 2
    }
    wx.request({
      url: app.util.url('/entry/wxapp/ApiGetOrders/'),
      method: 'GET',
      data: {},
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.data.errno == 0) {
          that.setData({
            orderList: res.data.data
          })
        } else {
          that.setData({
            orderList: null
          });
        }

      },
      fail: function (res) {
        if (res.data.errno == 0) {
          that.setData({
            orderList: res.data.data
          })
        } else {
          that.setData({
            orderList: null
          });
        }
      }
    })
  },
  onHide: function () {
  },
  onUnload: function () {
  },
  onPullDownRefresh: function () {
  },
  onReachBottom: function () {
  }
})