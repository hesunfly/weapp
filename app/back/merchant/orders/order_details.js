var app = getApp();
Page({
  data: {
    info: ''
  },
  onLoad: function (options) {
    var that = this;
    var orderid = options.orderid;
    wx.showToast({
      title: '获取数据中...',
      icon: 'loading',
      duration: 10000
    });
    wx.request({
      url: app.util.url('/entry/wxapp/Order/'),
      method: 'GET',
      data: {
        m: 'app', orderid: orderid
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
       if (res.data.errno == 0) {//下单时间规则
          var info = res.data.data;
          that.setData({
            info: info
          })
        }

      },
      fail: function () {
      },
      complete: function () {
        wx.hideToast();
      }
    })
  },
  //打电话
  tel: function (event) {
    wx.makePhoneCall({
      phoneNumber: event.target.dataset.phone
    })
  },
  cancelOrder: function (event) { //取消订单
    wx.showModal({
      title: '提示',
      content: '是否取消订单',
      success: function (res) {
        if (res.confirm) {
          var orderid = event.target.dataset.orderid;
          wx.showToast({
            title: '取消订单中...',
            icon: 'loading',
            duration: 10000
          });

          wx.request({
            url: app.util.url('/entry/wxapp/DelteOrder'),
            header: { "Content-Type": "application/x-www-form-urlencoded" },
            method: "POST",
            data: {
              m: 'app', orderId: orderid
            },
            success: function (res) {
                if (res.data.errno == 0) {
                  wx.showModal({
                    title: '提示',
                    content: '取消订单成功',
                    showCancel: false,
                    success: function (res) {
                      if (res.confirm) {
                        console.log('用户点击确定')
                        wx.navigateBack({
                          delta: 1
                        })
                      }
                    }
                  })
                } else {
                  wx.showModal({
                    title: '提示',
                    content: res.data.message,
                    showCancel: false,
                    success: function (res) {
                      if (res.confirm) {
                        console.log('用户点击确定')
                      }
                    }
                  })
                }

            },
            fail: function () {
            },
            complete: function () {
              wx.hideToast();
            }
          })
        }
      }
    })
  },
  pay: function (event) {//付款
    var orderid = event.target.dataset.orderid;
    wx.showToast({
      title: '付款中...',
      icon: 'loading',
      duration: 10000
    });

    wx.request({
      url: app.util.url('/entry/wxapp/PayByOrderId'),
      header: { "Content-Type": "application/x-www-form-urlencoded" },
      method: "POST",
      data: {
         m: 'app',
        orderId: orderid
      },
      success: function (res) {
                  if (res.data.errno == 0) {
                    var info = res.data.data;
                    wx.requestPayment({
                      'timeStamp': res.data.data.timeStamp,
                      'nonceStr': res.data.data.nonceStr,
                      'package': res.data.data.package,
                      'signType': 'MD5',
                      'paySign': res.data.data.paySign,
                      'success': function (_res) {
                        wx.showModal({
                          title: '提示',
                          content: '付款成功',
                          showCancel: false,
                          success: function ($res) {
                            if ($res.confirm) {
                              console.log('用户点击确定')
                              wx.navigateBack({
                                delta: 1
                              })
                            }
                          }
                        })
                      },
                      'fail': function (res) {
                        console.log(res)
                      }
                    })
                  } else {
                    wx.showModal({
                      title: '提示',
                      content: res.data.message,
                      showCancel: false,
                      success: function (res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        }
                      }
                    })
                  }

      },
      fail: function (res) {
        wx.showModal({
          title: '提示',
          content: res.data.message,
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
      },
      complete: function () {
        wx.hideToast();
      }

    })
  },
  previewImage: function (event) {
    wx.previewImage({
      current: event.target.dataset.url, // 当前显示图片的http链接
      urls: [event.target.dataset.url] // 需要预览的图片http链接列表
    })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})