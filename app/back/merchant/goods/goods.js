var app = getApp()
Page({
  data: {
    list: [],
    shopid:0
  },
  onLoad: function (options) {
    var shopid = options.shopid;
    this.setData({
      shopid: shopid
    })
  },
  onShow: function (options) {
    var that = this;
    wx.request({
      url: app.util.url('/entry/wxapp/ApiGetGoods/'),
      method: 'GET',
      data: {
        m: 'app', 'shopid': that.data.shopid
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {

         if (res.data.errno == 0) {
          that.setData({
            list: res.data.data
          })
        } else {
          that.setData({
            list: null
          });
        }
      }
    })
  },
  addCategory: function () {
    wx.navigateTo({
      url: '/app/pages/merchant/goods/edit?shopid='+this.data.shopid
    })
  },
  editCategory: function (e) {
    wx.navigateTo({
      url: "/app/pages/merchant/goods/edit?shopid=" + this.data.shopid + "&id=" + e.currentTarget.dataset.id + "&name=" + e.currentTarget.dataset.name
    })
  },
  delGoods: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (res) {
        if (res.confirm) {
          app.util.request({
            'url': 'entry/wxapp/ApiDelGoods',
            'data': {
              m: 'app', goodsid: id
            },
            'success': function (retus) {
              if (retus.data.errno == 0) {
                wx.hideLoading();
                wx.navigateBack({})
              }
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }

})
