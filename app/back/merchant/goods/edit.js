var app = getApp()
Page({
  data: {
    categorys:[],
    id: 0,
    imgArr: '',
    uploadimgArr: '',
    shopid: 0,
    curCate: null,// 当前选择的分类
    selectCid:0,
    switchVal:1,
    good_info:[]
  },
  onLoad: function (e) {
    var shopid = e.shopid; //
    var that = this;
    var id = e.id; //
    var that = this;
    that.setData({
      shopid: shopid,
      id: id,
    })

    wx.request({
      url: app.util.url('/entry/wxapp/ApiGetCategorys/'),
      method: 'GET',
      data: {
         m: 'app', 'shopid': shopid
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
      if (res.data.errno == 0) {
          that.setData({
            categorys: res.data.data
          })
        } else {
          that.setData({
            categorys: null
          });
        }

      }
    })

      wx.request({
        url: app.util.url('/entry/wxapp/ApiGetGood/'),
        method: 'GET',
        data: {
           m: 'app', 'shopid': shopid, 'goods_id': id
        },
        header: {
          'Accept': 'application/json'
        },
        success: function (res) {
         if (res.data.errno == 0) {
          that.setData({
            goods: res.data.data,
            imgArr: res.data.data.thumb
          })
        } else {
          that.setData({
            goods: null
          });
        }

        }
      })

  },
  bindCancel: function () {
    wx.navigateBack({})
  },
  //保存编辑
  confirm: function (e) {
    var that = this;
    var title = e.detail.value.title;
    if (title == "") {
      wx.showModal({
        title: '提示',
        content: '请填写商品名称',
        showCancel: false
      })
      return
    }
    var marketprice = e.detail.value.marketprice;
    if (title == "") {
      wx.showModal({
        title: '提示',
        content: '请填写商品名称',
        showCancel: false
      })
      return
    }
    if (that.data.curCate ==null) {
      wx.showModal({
        title: '提示',
        content: '请选择商品分类',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        }
      })
      return false;
    }
    var apiAddoRuPDATE = "add";
    var apiAddid = that.data.id;
    var uploadimgArr = this.data.uploadimgArr;
    if (apiAddid) {
      apiAddoRuPDATE = "update";
    } else {
      apiAddid = 0;
    }

    wx.request({
      url: app.util.url('/entry/wxapp/ApiSaveGoods/'),
      method: 'GET',
      data: {
        m: 'app', goods_id: apiAddid,
        shopid: that.data.shopid, status: that.data.switchVal,
        title: title, pcate: that.data.curCate.id, marketprice: marketprice, sellnums: e.detail.value.sellnums,
        description: e.detail.value.description, thumb: uploadimgArr
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.data.errno != 0) {
          wx.hideLoading();
          wx.showModal({
            title: '失败',
            content: res.data.message,
            showCancel: false
          })
          return;
        }else{
          wx.showModal({
            title: '提示',
            content: '提交成功'  ,showCancel: false
          })
          wx.navigateBack({})
        }

      }
    })
  },
  deleteAddress: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.util.url('/entry/wxapp/apiDelAddress/'),
            method: 'GET',
            data: {
               m: 'app', addressid: id
            },
            header: {
              'Accept': 'application/json'
            },
            success: function (res) {
             if (retus.data.errno == 0) {
                wx.hideLoading();
                wx.navigateTo({
                  url: '/app/pages/address/index'
                })
              }

            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  bindChangeCid: function (e) {
    const selIndex = e.detail.value[0] - 1;
    if (selIndex == -1) {
      this.setData({
        curCate: null
      });
      return;
    }
    this.setData({
      curCate: this.data.categorys[selIndex]
    });
    console.log(this.data.curCate);
  },
  switch1Change: function (e) {
    let val = e.detail.value ;
    var selVal = val==true ? 1 : 0;
    console.log(selVal==true);
    this.setData({
      switchVal: selVal
    })
    console.log('switch1 发生 change 事件，携带值为', val)
  },
   addImg: function () {
    var that = this;
    var imgArr = this.data.imgArr;
    var uploadimgArr = this.data.uploadimgArr;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        wx.showToast({
          title: '正在上传...',
          icon: 'loading',
          duration: 10000
        });
        var uploadurl = app.util.url('entry/wxapp/WxUpload');
        wx.uploadFile({
          url: uploadurl + "m=app",
          filePath: tempFilePaths[0],
          name: 'file',
          formData: {},
          success: function (res) {
            console.log(res.data);
            var data = res.data;
            if (typeof data === 'string') data = JSON.parse(data.trim());//解压缩
            if (data.errno == 0) { //成功
              imgArr = tempFilePaths[0];
              uploadimgArr = data.data;
              that.setData({
                imgArr: imgArr,
                uploadimgArr: uploadimgArr
              })
            } else {
              wx.showToast({
                title: data.message,
                icon: 'success',
                duration: 2000
              })
            }
          },
          complete: function () {
            wx.hideToast();
          }
        })
      }
    })
  }
})
