var app = getApp();
var server = require('../../../resource/utils/server.js');
Page({
  data: {
    imgArr: '',
    imgArr2: '',
    merchant: [], id: 0,
    uploadimgArr: '',
    uploadimgArr2: '',
    textareaVal: '', addressVal: '',
    categorys: [], selectval: [],
    inputVal: '', locationAddress: '',
  },
  onLoad: function (options) {
    // 实例化API核心类
    let that = this
    // qqmapsdk = new QQMapWX({
    //   key: app.globalData.map_ak == "" ? 'YGRBZ-4SPKS-YQ5OO-6XUPS-VH73S-Y6BP6' : app.globalData.map_ak
    // });
  },
  onShow: function () {
    let that = this
    wx.request({
      url: app.util.url('/entry/wxapp/apiGetShopMine/'),
      method: 'GET',
      data: {
        m: 'app'
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
      if (res.data.errno == 0) {
          var me = res.data.data.merchant;
          let list = res.data.data.list;
          if (me.id > 0 && me.is_notice != undefined && me.is_notice == 1) {
            that.setData({
              merchant: merchant,
              locationAddress: me.address,
              lat: me.lat,
              lng: me.lng,
              imgArr: me.logo,
              imgArr2: me.yyimg,
              categorys: list
            })
          } else if (me.id <= 0) {

          } else {
            wx.navigateTo({
              url: '/app/pages/merchant/index/index?id=' + me.id + "&shopname=" + me.title,
            })
          }
          that.setData({
            categorys: list,
            id: me.id
          })
        }

      },
      fail: function () {
      }
    })
  },
  //添加图片
  addImg: function () {
    var that = this;
    var imgArr = this.data.imgArr;
    var uploadimgArr = this.data.uploadimgArr;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        //上传图片
        wx.showToast({
          title: '正在上传...',
          icon: 'loading',
          duration: 10000
        });
        var uploadurl = app.util.url('entry/wxapp/WxUpload');
        wx.uploadFile({
          url: uploadurl + "m=app",
          filePath: tempFilePaths[0],
          name: 'file',
          formData: {},
          success: function (res) {
            console.log(res.data);
            var data = res.data;
            if (typeof data === 'string') data = JSON.parse(data.trim());//解压缩
            if (data.errno == 0) { //成功
              imgArr = tempFilePaths[0];
              uploadimgArr = data.data;
              that.setData({
                imgArr: imgArr,
                uploadimgArr: uploadimgArr
              })
            } else {
              wx.showToast({
                title: data.message,
                icon: 'success',
                duration: 2000
              })
            }
          },
          complete: function () {
            wx.hideToast();
          }
        })
      }
    })
  },
  addImg2: function () {
    var that = this;
    var imgArr2 = this.data.imgArr2;
    var uploadimgArr2 = this.data.uploadimgArr2;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        //上传图片
        wx.showToast({
          title: '正在上传...',
          icon: 'loading',
          duration: 10000
        });
        var uploadurl = app.util.url('entry/wxapp/WxUpload');
        wx.uploadFile({
          url: uploadurl + "m=app",
          filePath: tempFilePaths[0],
          name: 'file',
          formData: {},
          success: function (res) {
            var data = res.data;
            if (typeof data === 'string') data = JSON.parse(data.trim());//解压缩
            if (data.errno == 0) { //成功
              imgArr2 = tempFilePaths[0];
              uploadimgArr2 = data.data;
              that.setData({
                imgArr2: imgArr2,
                uploadimgArr2: uploadimgArr2
              })
            } else {
              wx.showToast({
                title: data.message,
                icon: 'success',
                duration: 2000
              })
            }
          },
          complete: function () {
            wx.hideToast();
          }
        })
      }
    })
  },
  imgDel: function (event) {
    var imgArr = this.data.imgArr;
    /**var index = event.target.dataset.index;
    if (imgArr.length) {
      for (var i = 0; i < imgArr.length; i++) {
        if (i == index) {
          imgArr.splice(i, 1);
        }
      }
    }**/
    imgArr = '';
    this.setData({
      imgArr: imgArr
    })
  },
  imgDel2: function (event) {
    var imgArr2 = this.data.imgArr2;
    /**var index = event.target.dataset.index;
    if (imgArr.length) {
      for (var i = 0; i < imgArr.length; i++) {
        if (i == index) {
          imgArr.splice(i, 1);
        }
      }
    }**/
    imgArr2 = '';
    this.setData({
      imgArr2: imgArr2
    })
  },
  previewImage: function (event) {
    wx.previewImage({
      current: event.target.dataset.url, // 当前显示图片的http链接
      urls: [event.target.dataset.url] // 需要预览的图片http链接列表
    })
  },
  previewImage2: function (event) {
    wx.previewImage({
      current: event.target.dataset.url, // 当前显示图片的http链接
      urls: [event.target.dataset.url] // 需要预览的图片http链接列表
    })
  },
  textareaVal: function (event) {//问题和建议描述
    this.setData({
      textareaVal: event.detail.value
    })
  },
  inputVal: function (event) { //联系电话
    this.setData({
      inputVal: event.detail.value
    })
  },
  //提交
  confirm: function (e) {
    var notice = e.detail.value.shop_notice;
    var shop_tel = e.detail.value.shop_tel;
    var shop_name = e.detail.value.shop_name; // 备注信息
    var start_price = e.detail.value.start_price; //
    var send_price = e.detail.value.send_price; //
    var yytime = e.detail.value.yytime;
    var address = e.detail.value.address;
    var uploadimgArr = this.data.uploadimgArr;
    var yyimg = this.data.uploadimgArr2;
    var formid = e.detail.formId;
    var room_list = e.detail.value.room_list;
    var username = e.detail.value.username;
    var longitude = e.detail.value.longitude;
    var latitude = e.detail.value.latitude;
    var pwd = e.detail.value.pwd;
    var apiAddid = this.data.id;
    var seletd = this.data.selectval
    seletd = seletd.join("|");
    if (apiAddid) {
    } else {
      apiAddid = 0;
    }
    if (!shop_name) {
      wx.showModal({
        title: '提示',
        content: '请填写您的商户名称',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        }
      })
      return false;
    }
    // if (inputVal) {
    //   if (!utils.regularPhoneNumber(inputVal)) {
    //     wx.showModal({
    //       title: '提示',
    //       content: '手机号码格式不对',
    //       showCancel: false,
    //       success: function (res) {
    //         if (res.confirm) {
    //           console.log('用户点击确定')
    //         }
    //       }
    //     })
    //     return false;
    //   }
    // }
    wx.showToast({
      title: '提交中...',
      icon: 'loading',
      duration: 10000
    });


      wx.request({
        url: app.util.url('/entry/wxapp/ApiSaveShop/'),
        header: { "Content-Type": "application/x-www-form-urlencoded" },
        method: "POST",
        data: {
          m: 'app',
        shop_name: shop_name,
        tel: shop_tel, apiAddid: apiAddid,
        notice: notice, imgs: uploadimgArr, cid: seletd,
        start_price: start_price, formid: formid, address: address,
        send_price: send_price, yytime: yytime, longitude: longitude, latitude: latitude, room_list: room_list,
        username: username, pwd: pwd, yyimgs: yyimg
        },
        success: function (res) {
           console.log(res);
                  if (res.data.errno == 0) {
                    wx.showModal({
                      title: '提示',
                      content: res.data.message,
                      showCancel: false,
                      success: function (res) {
                        if (res.confirm) {
                          wx.navigateBack({
                            delta: 1
                          })
                        }
                      }
                    })
                  } else {
                    wx.showModal({
                      title: '提示',
                      content: res.data.message,
                      showCancel: false,
                      success: function (res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        }
                      }
                    })
                  }

          },
          fail: function (res) {
            wx.showModal({
              title: '提示',
              content: res.data.message,
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                }
              }
            })
          },
          complete: function () {
            wx.hideToast();
          }

      })


  },
  chooseLocation: function () {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          hasLocation: true,
          lng: res.longitude,
          lat: res.latitude,
          locationAddress: res.address
        })
      }
    })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  checkboxChange: function (e) {
    let that = this;
    var arr = e.detail.value;
    that.setData({
      selectval: arr
    })
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
  }
})