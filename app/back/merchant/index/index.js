var app = getApp();
Page({
  data: {
    userInfo: {},
    bbs_show_status: true,
    shop_show_status: true,
    menu_list: '',
    shopname:'',
    shopid: '',
    is_loaded: false,
    has_coupon: true,
    is_open_card: false,
  },
  onLoad: function (options) {
    this.onPullDownRefresh();
    let shopid  = options.id;
    let shopname = options.shopname; 
    this.setData({
      shopid: shopid,
      shopname: shopname
    })
  }, 
  onPullDownRefresh: function () { 
  },   
  /**
  * 跳转页面
  */
  onNavigateTap: function (e) {
    const dataset = e.currentTarget.dataset, url = dataset.url, name = dataset.name;
    if ("wechat_info_sync" == name) {
      this.onSyncWechatInfo();
    } else if ("wechat_address" == name) {
      wx.chooseAddress({});
    } else if ("wechat_setting" == name) {
      wx.openSetting({});
    } else if ("wechat_clear" == name) {
      wx.showToast({ title: '正在清理中...', icon: 'loading', duration: 10 });
      wx.clearStorageSync();
      wx.showToast({ title: '清理完成', icon: 'success', duration: 1500 });
    } else {
      wx.navigateTo({ url: url })
    }
  },

  /**
   * 同步微信信息
   */
  onSyncWechatInfo: function () {
    if (requestUtil.isLoading(this.syncWechatInfoId)) return;
    util.getUserInfo((info) => {
      //保存用户信息
      this.syncWechatInfoId = requestUtil.post(API_USER_INFO_SAVE_URL, {
        nickname: info.nickName,
        headimgurl: info.avatarUrl,
        sex: info.gender,
        city: info.city, province: info.province,
        country: info.country, language: info.language,
      }, (data) => {
        console.log(data);
        wx.showToast({
          title: '同步成功！',
          icon: 'success',
          duration: 2000
        });
        const userInfo = _.extend(this.data.userInfo || {}, data);
        this.setData({ userInfo: userInfo });
      });
    });
  },

  /**
   * 展开或收缩
   */
  onToggleTap: function (e) {
    const dataset = e.currentTarget.dataset, name = dataset.name;
    const item = _.find(this.data.menus, { name: name });
    if (!item) return;
    item.isshow = !item.isshow;
    this.setData({ menus: this.data.menus });
  },
});