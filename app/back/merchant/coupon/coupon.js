var app = getApp()
Page({
  data: {
    list: [],
    shopid:0
  },
  onLoad: function (options) {
    var shopid = options.shopid;
    this.setData({
      shopid: shopid
    })
  },
  onShow: function (options) {
    var that = this;

    wx.request({
      url: app.util.url('/entry/wxapp/ApiGetCategorys/'),
      method: 'GET',
      data: {
        m: 'app', 'shopid': that.data.shopid
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
       if (res.data.errno == 0) {
          that.setData({
            list: res.data.data
          })
        } else {
          that.setData({
            list: null
          });
        }

      }
    })
  },
  addCategory: function () {
    wx.navigateTo({
      url: '/app/pages/merchant/category/edit?shopid='+this.data.shopid
    })
  },
  editCategory: function (e) {
    wx.navigateTo({
      url: "/app/pages/merchant/category/edit?shopid=" + this.data.shopid + "&id=" + e.currentTarget.dataset.id + "&name=" + e.currentTarget.dataset.name
    })
  },

})
