var app = getApp()
Page({
  data: {
  },
  onLoad: function (options) {

  },
  onReady: function () {
    wx.setNavigationBarColor({
      frontColor: app.globalData.title_color,
      backgroundColor: app.globalData.color,
    })
    const self = this;
    app.util.footer(self);
  },
  onShow: function () {// 获取订单列表
    wx.showLoading();
    var that = this;
    var postData = { 'm': 'app' };

    wx.request({
      url: app.util.url('/entry/wxapp/likes/'),
      method: 'GET',
      data:postData,
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        if (res.data.errno == 0) {
          that.setData({
            orderList: res.data.data
          })
        } else {
          that.setData({
            orderList: null
          });
        }

      },
      'fail': function (res) {
        if (res.data.errno == 0) {
          that.setData({
            orderList: res.data.data
          })
        } else {
          that.setData({
            orderList: null
          });
        }
      }
    })
  }
})