var getSetting = function(app) {
    //获取平台信息
    var that = this;

    wx.request({
        url: app.globalData.url + 'setting/app',
        data: {},
        header: {
            'content-type': 'application/json' // 默认值
        },
        method: 'get',
        success: function(res) {
            app.globalData.title = res.data.title
            app.globalData.bar_color = res.data.bar_color
            app.globalData.title_color = res.data.title_color
            app.globalData.copyright = res.data.copyright
            app.globalData.support = res.data.support
          
            wx.setNavigationBarTitle({
                title: res.data.title
            })
            commonSet(app)
        }
    })
}
var commonSet = function(app) {

    wx.setNavigationBarColor({
        frontColor: app.globalData.title_color,
        backgroundColor: app.globalData.bar_color,
    })
}
module.exports = {
    getSetting: getSetting,
    commonSet: commonSet
}