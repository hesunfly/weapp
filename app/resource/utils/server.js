function formatTime(date) {
  const year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds();

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function isEmptyObject(obj) {
  if ((typeof obj === "object" && !(obj instanceof Array)) || ((obj instanceof Array) && obj.length <= 0)) {
    var isEmpty = true;
    for (var prop in obj) {
      isEmpty = false;
      break;
    }
    return isEmpty;
  }
  return false;
}

function filterEmptyObject(list) {
  var cartList = [];
  for (var index in list) {
    if (!this.isEmptyObject(list[index])) {
      cartList.push(list[index])
    }
  }
  return cartList;
}

function selectedShopDetail(shopId) {
  var app = getApp();
  for (var i = 0; i < app.globalData.shops.length; ++i) {
    if (app.globalData.shops[i].id == shopId) {
      return app.globalData.shops[i]
    }
  }
  return null;
}


//判断手机号码是否正确
function regularPhoneNumber(str) {
  var s = str.replace(/\s|\-/g, '');
  if (s.indexOf("+86") == 0) {
    s = s.substr(3);
  }
  console.log(s)
  // var d = /^1\d{20}$/g;
  var d = /^1[3|4|5|7|8][0-9]\d{11}$/;
  if (d.test(s)) {
    return s;
  } else {
    return null;
  }
}
module.exports = {
  formatTime: formatTime,
  isEmptyObject: isEmptyObject,
  selectedShopDetail: selectedShopDetail,
  regularPhoneNumber: regularPhoneNumber,
  filterEmptyObject: filterEmptyObject
}
