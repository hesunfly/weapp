App({
    data: {
        session: [],
    },
    //当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
    onLaunch: function() {
        var that = this
        that.checkToken
        // this.refresh

    },

    
    login: function() {
        var that = this
        wx.login({
            success: function(res) {
                wx.request({
                    url: that.globalData.url + 'auth/weLogin',
                    data: {
                        code: res.code
                    },
                    method: 'post',
                    header: {
                        'content-type': 'application/json' // 默认值
                    },
                    success: function(ress) {
                        console.log(ress);
                        if(ress.statusCode == 201){                
                            wx.setStorageSync('access_token', ress.data.access_token)
                            wx.setStorageSync('access_token_expired_at', new Date().getTime() + ress.data.expires_in * 1000)                            
                            wx.showToast({
                                title: '登录成功',
                                icon: 'success',
                                duration: 2000
                            })
                            return ress
                        }else{
                            wx.showToast({
                                title: '登录失败',
                                icon: 'none',
                                duration: 2000
                            })
                        }
                    }
                })
            }
        })

    },

    refreshToken: function(accessToken) {
        var that = this
        wx.request({
            url: that.globalData.url + 'auth/refresh',
            data: {

            },
            method: 'put',
            header: {
                'Authorization': 'Bearer ' + accessToken
            },
            success: function(ress) {
                wx.setStorageSync('access_token', ress.data.access_token)
                wx.setStorageSync('access_token_expired_at', new Date().getTime() + ress.data.expires_in * 1000)
                return ress
            }
        })
    },
    getToken:function(){
        var that = this
        let accessToken = wx.getStorageSync('access_token')
        let expiredAt = wx.getStorageSync('access_token_expired_at')
        // 如果 token 过期了，则调用刷新方法
        if (accessToken && new Date().getTime() > expiredAt) {
            var refresh = that.refreshToken(accessToken)
            if (refresh.statusCode == 200) {
                accessToken = refresh.data.accessToken
            } else {
                var login = that.login()
                if (login.statusCode == 201) {
                    accessToken = login.data.accessToken
                }
            }
        }
        return accessToken
    },
    http: function (_url, _data, _method, callback) {
    
        var that = this;
        wx.request({
            url: this.globalData.url + _url,
            method: _method,
            data: _data,
            success: callback,

            fail: function (res) {
                console.log(res);
                console.log('出错了');
            }
        })

    },


    _http: function (_url, _data, _method, callback, callbackFail) {
        // 从缓存中取出 Token
        var that = this;

        var accessToken = that.getToken()
        wx.request({
            url: this.globalData.url + _url,
            header: {
                'Authorization': 'Bearer ' + accessToken
            },
            method: _method,
            data: _data,
            success: callback,

            fail: callbackFail
        })

    },
    
    checkToken:function(){
        var that = this
        let token = wx.getStorageSync('access_token');
        if (!token) {
            that.login()
        } else {
            console.log('token=' + token);
        }
    },
    //当小程序从前台进入后台，会触发 onHide
    onHide: function() {},
    // 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
    onError: function(msg) {
        // console.log(msg)
    },
    //当小程序启动，或从后台进入前台显示，会触发 onShow
    onShow: function() {

    },
    globalData: {
        hasLogin: false,
        cartList: [],
        openid: '',
        userInfo: [],
        url: 'http://api.wxapp.test/',
        // url: 'http://192.168.1.34/api/',
        // url: 'https://wxapp.ihege.top/api/',
        resource: 'http://api.wxapp.test',
        // resource: 'http://192.168.1.34',
        // resource: 'https://wxapp.ihege.top',
        title: '',
        bar_color: '',
        title_color: '',
        copyright: '',
        support: '',
        accessToken: ''
    },
    tools: require('app/resource/utils/tools.js'),
});